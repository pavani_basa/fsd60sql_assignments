1. Insert a record by accepting the employee details from runtime

``` package day1;

import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class InsertRun {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;
		Scanner sc = new Scanner(System.in);
		int empId = sc.nextInt();
		String empname = sc.next();
		double salary = sc.nextDouble();
		String gender = sc.next();
		String email = sc.next();
		String password = sc.next();
		String insertQuery = "insert into employee2 values(" + empId + ", ' " + empname + " ', " + salary + ", ' " + gender + " ',' " + email + " ',' " + password + " ')";
		
		try {
			st = con.createStatement();
			// dml = executeupdate - insert, update, delete
			// dql = executequery
			//
			int res = st.executeUpdate(insertQuery);
			if(res > 0) {
				System.out.print(res);
			}else {
				System.out.print("failed");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(con != null) {
			st.close();
			con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
```

2. Fetch the employee record based on empId;
3. Show All Employee Id's, Fetch the employee record based on empId;

```
package day1;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

//Get All Records from Employee Table
public class Select {
	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
//		Connection connection1 = DbConnection.getConnection();
		Statement statement = null;
//		Statement statement1 = null;
		ResultSet resultSet = null;
//		ResultSet resultSet1 = null;
		ArrayList<Object> resemp = new ArrayList<Object>();
//	    java.util.Scanner sc = new java.util.Scanner(System.in);
//	    int empid = sc.nextInt();

		String selectQuery = "select * from employee2";
		

		try {
			statement = connection.createStatement();
//			statement1 = connection1.createStatement();
			resultSet = statement.executeQuery(selectQuery);
			

			if (resultSet != null) {

				while(resultSet.next()) {

					resemp.add(resultSet.getInt(1));
					

				}

			} else {
				System.out.println("No Record(s) Found!!!");
			}
			 Scanner scanner = new Scanner(System.in);
	            int empid = scanner.nextInt();

	            if (resemp.contains(empid)) {
	                try (Connection connection2 = DbConnection.getConnection();
	                     Statement statement2 = connection2.createStatement()) {

	                    String selectQuery2 = "SELECT * FROM employee2 WHERE empid = " + empid;
	                    ResultSet resultSet2 = statement2.executeQuery(selectQuery2);
	                    while (resultSet2.next()) {
	                       
	                        System.out.println("ID: " + resultSet2.getInt(1));
	                        System.out.println("Name: " + resultSet2.getString(2));
	                        System.out.println("salary " + resultSet2.getDouble(3));
	                        System.out.println("salary " + resultSet2.getString(4));
	                        System.out.println("email: " + resultSet2.getString(5));
	                        System.out.println("password: " + resultSet2.getString(6));
	                        
	                    }
	                }
	            } else {
	                System.out.println("ID not found.");
	            }
//			if(resemp.contains(empid)) {
//				
//				while(resultSet1.next()) {
//					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
//					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
//					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
//				
//					
//				}
//			}
//			else {
//				System.out.println("Record is not addded in table");
//			}
//			
			

		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				
				statement.close();
//				statement1.close();
				
				connection.close();
//				connection1.close();
			}
		} catch (SQLException e) {
			System.out.println("No Record(s) Found!!!");
			
			e.printStackTrace();
		}

	}
}

```

4. All the programs should be communicating with MySQL Database from ByetHost.com

```
package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbByetConn {
	public static Connection getConnection() {
		Connection con = null;
		String url = "jdbc:mysql://sql302.byethost14.com/b14_35929318_bts";
		
		try {
		
			//1. Loading the MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			
			//2. Establishing the Connection
			con = DriverManager.getConnection(url, "b14_35929318", "ypur_password");
			
			
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
}
```

```
package day1;

import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbByetConn;


public class InsertByet {

	public static void main(String[] args) {
		Connection con = DbByetConn.getConnection();
		if(con != null) {
			System.out.println("connected");
		}
		else {
			System.out.println("not connected");
		}
		Statement st = null;
		Scanner sc = new Scanner(System.in);
		int id = sc.nextInt();
		String name = sc.next();
		String profession = sc.next();
		int salary = sc.nextInt();
		String insertQuery = "insert into bts1 values(" + id + ", ' " + name + " ', " + ", ' " + profession + " ',' "+ salary +" ')";
		
		try {
			st = con.createStatement();
			// dml = executeupdate - insert, update, delete
			// dql = executequery
			//
			int res = st.executeUpdate(insertQuery);
			if(res > 0) {
				System.out.print(res);
			}else {
				System.out.print("failed");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(con != null) {
			st.close();
			con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}

```