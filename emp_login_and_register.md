1. Login form and Servlet:

```
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login form</title>
</head>
<body>
    <form action = "LoginServlet">
      <input type = "email" name = "email" /> <br />
      <input type = "password" name = "password" /> <br />
      <button> Login </button>
    </form>

</body>
</html>
```

```
package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		if(email.equalsIgnoreCase("pav@gmail.com") && password.equals("1234")) {
			out.println("Login successful");
		}
		else {
			out.println("Login not successful");
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

```

2. Register form and servlet

```
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Form</title>
    
</head>
<body>

<form action="RegisterServlet">
    <h2>Registration Form</h2>
    <table>
        <tr>
            <th><label for="name">Name:</label></th>
            <td><input type="text" id="name" name="name" required></td>
        </tr>
        <tr>
            <th><label for="id">ID:</label></th>
            <td><input type="text" id="id" name="id" required></td>
        </tr>
        <tr>
            <th><label for="password">Password:</label></th>
            <td><input type="password" id="password" name="password" required></td>
        </tr>
        <tr>
            <th><label for="email">Email:</label></th>
            <td><input type="email" id="email" name="email" required></td>
        </tr>
        <tr>
            <th><label for="salary">Salary:</label></th>
            <td><input type="text" id="salary" name="salary" required></td>
        </tr>
        <tr>
            <th><label for="gender">Gender:</label></th>
            <td>
                <select id="gender" name="gender" required>
                    <option value="">Select Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Other</option>
                </select>
            </td>
        </tr>
    </table>
    <button> Register </button>
</form>

</body>
</html>
```

```
package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String salary = request.getParameter("salary");
		String gender = request.getParameter("gender");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		out.println("Register successful");
		out.println("Details of the entered user are");
		out.println(id);
		out.println(name);
		out.println(gender);
		out.println(salary);
		out.println(email);
		out.println(password);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}

```